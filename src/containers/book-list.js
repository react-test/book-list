import React, { Component } from 'react'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { selectBook } from '../actions/index';

class BookList extends Component {

    constructor(props) {
        super(props);

    }

    renderList() {
        return this.props.books.map(book => {
            console.log(book);
            return (
                <li
                    className="list-group-item"
                    key={book.title}
                    onClick={() => this.props.selectBook(book)}>
                    {book.title}
                </li>
            );
        });

    }

    render() {
        return (
            <ul className="list-group col-sm-4">

                {this.renderList()}
            </ul>
        );
    }
}
function mapStateToProps(state) {
    return {
        books: state.books
    };
}

function mapDispatchToPorps(dispatch) {
    return bindActionCreators({ selectBook }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToPorps)(BookList);
